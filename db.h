#ifndef DB_H
#define DB_H
#include <time.h>
#include <stdbool.h>

#include "vector.h"
#include "stack.h"

enum e_sport {RUNNING, BIKING, OTHER};
enum e_state {UNDEFINED_S, DATABASE_S, ACTIVITY_S, ACTIVITY_ID_S, LAP_S, TOTALTIME_S, DISTANCE_S, CALORIES_S, AVGHR_S, MAXHR_S, AVGHR_V_S, MAXHR_V_S, TRACK_S, 
	TRACKPOINT_S, TRACKPOINT_HR_S, TRACKPOINT_HR_V_S, TRACKPOINT_TIME_S, TRACKPOINT_POS_S, TRACKPOINT_LAT_S, TRACKPOINT_LON_S, TRACKPOINT_ALT_S, TRACKPOINT_DIST_S};

#define PARSER_BUF_SIZE 512

#define DATABASE_TAG "TrainingCenterDatabase"
#define ACTIVITY_TAG "Activity"
#define ID_TAG "Id"
#define LAP_TAG "Lap"
#define TOTALTIME_TAG "TotalTimeSeconds"
#define DISTANCE_TAG "DistanceMeters"
#define CALORIES_TAG "Calories"
#define AVGHR_TAG "AverageHeartRateBpm"
#define MAXHR_TAG "MaximumHeartRateBpm"
#define VALUE_TAG "Value"
#define TRACK_TAG "Track"
#define TRACKPOINT_TAG "Trackpoint"
#define TIME_TAG "Time"
#define HR_TAG "HeartRateBpm"
#define ALTITUDE_TAG "AltitudeMeters"
#define POSITION_TAG "Position"
#define LAT_TAG "LatitudeDegrees"
#define LON_TAG "LongitudeDegrees"

#define TIME_FMT "%Y-%m-%dT%XZ"

struct s_geocoord {
	double lat;
	double lon;
};

struct s_trackpoint {
	struct tm time;
	struct s_geocoord position;
	double altitude;
	double distance;
	int heart_rate;
};
VECTOR_DECL(tp, struct s_trackpoint);

struct s_track {
	Vector_tp points;
};

struct s_lap {
    struct tm start_time;
    double total_time_sec;
    double distance_meter;
    int calories;
	int avg_hr;
	int max_hr;
	struct s_track track;
};
VECTOR_DECL(lap, struct s_lap)

struct s_activity {
    struct tm id;
	enum e_sport sport;
    Vector_lap laps;
};

VECTOR_DECL(act, struct s_activity)

struct s_database {
	Vector_act activities;
    Stack_i state;
	char parser_buffer[PARSER_BUF_SIZE];
};

void init_db(struct s_database* db);
void deinit_db(struct s_database* db);

enum e_state db_get_state(const struct s_database* db);
void db_push_state(struct s_database* db, enum e_state new_state);
void db_pop_state(struct s_database* db);

struct s_activity* db_last_activity(struct s_database* db);
struct s_lap* db_last_lap(struct s_database* db);
struct s_track* db_last_track(struct s_database* db);
struct s_trackpoint* db_last_tp(struct s_database* db);

const char* activity_sport_name(enum e_sport);
void print_lap(const struct s_lap* lap);

void deinit_activity(size_t, struct s_activity* activity);
void deinit_lap(size_t, struct s_lap* lap);
void deinit_track(size_t, struct s_track* track);

#endif
