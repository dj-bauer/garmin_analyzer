#ifndef GARMIN_PARSER_H
#define GARMIN_PARSER_H
#include <stdbool.h>
#include "db.h"

bool try_tag(const char* tag, const char* searching, struct s_database* db, enum e_state old_state, enum e_state new_state);
bool try_close_tag(const char* tag, const char* searcing, struct s_database* db, enum e_state expected_state);

#endif
