#define _XOPEN_SOURCE 700
#include "db.h"
#include "vector_impl.h"
#include "util.h"

#include <stdio.h>

VECTOR_IMPL(act, struct s_activity)
VECTOR_IMPL(lap, struct s_lap)
VECTOR_IMPL(tp, struct s_trackpoint)

void init_db(struct s_database* db)
{
	init_vector_act(&db->activities);
    init_stack_i(&db->state);
	db->parser_buffer[0] = '\0';
}

void deinit_db(struct s_database* db)
{
    deinit_stack_i(&db->state);
    vector_foreach_act(&db->activities, deinit_activity);
	deinit_vector_act(&db->activities);
}
void deinit_activity(size_t i, struct s_activity* activity)
{
    vector_foreach_lap(&activity->laps, deinit_lap);
    deinit_vector_lap(&activity->laps);
}
void deinit_lap(size_t i, struct s_lap* lap)
{
	deinit_track(0, &lap->track);
}
void deinit_track(size_t, struct s_track* track)
{
	deinit_vector_tp(&track->points);
}

enum e_state db_get_state(const struct s_database* db)
{
    if(stack_size_i(&db->state) == 0)
        return UNDEFINED_S;
    return *stack_top_i((Stack_i*) &db->state);
}
void db_push_state(struct s_database* db, enum e_state new_state)
{
    stack_push_i(&db->state, new_state);
}
void db_pop_state(struct s_database* db)
{
    stack_pop_i(&db->state);
}

struct s_activity* db_last_activity(struct s_database* db)
{
    return vector_last_act(&db->activities);
}
struct s_lap* db_last_lap(struct s_database* db)
{
    return vector_last_lap(&db_last_activity(db)->laps);
}
struct s_track* db_last_track(struct s_database* db)
{
	return &db_last_lap(db)->track;
}
struct s_trackpoint* db_last_tp(struct s_database* db)
{
	return vector_last_tp(&db_last_track(db)->points);
}

const char* activity_sport_name(enum e_sport sport)
{
    switch(sport) {
        case RUNNING:
            return "Running";
        case BIKING:
            return "Biking";
        case OTHER:
            return "Undefined";
    }
}

void print_tp(size_t, struct s_trackpoint* tp)
{
    char dist_str[DIST_BUF_LEN];
    fmt_dist(&dist_str, tp->distance);
	printf("\t\t\tDist: %s HR: %dBpm Alt: %.2lfm %s", dist_str, tp->heart_rate, tp->altitude, asctime(&tp->time));
	printf("\t\t\t\tLat: %lf Lon: %lf\n", tp->position.lat, tp->position.lon);
}
void print_lap(const struct s_lap* lap)
{
    char time_str[TIME_BUF_LEN];
    char dist_str[DIST_BUF_LEN];
    fmt_dur(&time_str, lap->total_time_sec);
    fmt_dist(&dist_str, lap->distance_meter);
    printf("\tLap Start: %s", asctime(&lap->start_time));
    printf("\t\tTime: %s\n", time_str);
    printf("\t\tDistance: %s\n", dist_str);
    printf("\t\tCalories burned: %dkcal\n", lap->calories);
	printf("\t\tHeart Rate: %d avg / %d max\n", lap->avg_hr, lap->max_hr);
	printf("\t\tTrack:\n");
	vector_foreach_tp((Vector_tp*) &lap->track.points, print_tp);
}
