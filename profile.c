#include "profile.h"

void calc_pulse_zones(struct s_profile* profile, 
		int resting_pulse, int max_pulse)
{
	profile->resting_pulse = resting_pulse;
	profile->max_pulse = max_pulse;
	double variance = max_pulse - resting_pulse;
	double fractions[] = {0.6, 0.7, 0.8, 0.9, 1.0};
	for(int i=0; i<5; i++) {
		profile->zones.zone_max[i] = resting_pulse + variance * fractions[i];
	}

}
