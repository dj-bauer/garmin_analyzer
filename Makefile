CFLAGS=-I/home/dbauer/repos/cstdj/include -I/home/dbauer/repos/cstdj/src -g
LDFLAGS=-L/home/dbauer/repos/cstdj/ -lstdj -fsanitize=address -lc

SOURCES=main.c db.c util.c parser.c analyzer.c profile.c
all:
	${CC} ${CFLAGS} ${LDFLAGS} ${SOURCES} -lexpat -o garmin
