#include "analyzer.h"
#include "logger.h"
#include "profile.h"
#include "util.h"

int pulse_zone(struct s_profile* profile, int rate)
{
	for(int i=3; i>-1; i--) {
		if(profile->zones.zone_max[i] < rate) {
			return i+1;
		}
	}
	return 0;
}

void analyze(struct s_statistics* stat, StatType type, void* device, struct s_profile* profile)
{
	stat->type = type;
	stat->profile = profile;
	for(int i=0; i<5; i++) {
		stat->zonestats[i] = (struct s_zoneinfo){0,0};
	}
	switch(stat->type) {
		case LapStat: 
			stat->lap = device; 
			for(int i=1; i<vector_size_tp(&stat->lap->track.points); i++) {
				struct s_trackpoint* tp = vector_at_tp(&stat->lap->track.points, i);
				struct s_trackpoint* prev = vector_at_tp(&stat->lap->track.points, i-1);
				if(tp->distance == 0)
					continue;
				double dist_delta = tp->distance - prev->distance;
				double time_delta = timediff(tp->time, prev->time);
				int zone = pulse_zone(profile, tp->heart_rate);
				stat->zonestats[zone].distance += dist_delta;
				stat->zonestats[zone].time += time_delta;
			}
			break;
		case ActivityStat: stat->activity = device; break;
	}
}

void print_stat(struct s_statistics* stat)
{
	fprint_stat(stdout, stat);
}

void fprint_stat(FILE* f, struct s_statistics* stat)
{
	switch(stat->type) {
		case LapStat:
			struct s_lap* lap = stat->lap;
			char zone_dists[5][DIST_BUF_LEN];
			char zone_time[5][TIME_BUF_LEN];
			for(int i=0; i<5; i++) {
				fmt_dist(&zone_dists[i], stat->zonestats[i].distance);
				fmt_dur(&zone_time[i], stat->zonestats[i].time);
			}
			fprintf(f, "\033[1mLap %s\033[0m ran by %s\n", 
					asctime(&lap->start_time), stat->profile->name);
			fprintf(f, "╭────────┬─────────┬─────────┬─────────┬─────────┬─────────╮\n");
			fprintf(f, "│        │ Zone 1  │ Zone 2  │ Zone 3  │ Zone 4  │ Zone 5  │\n");
			fprintf(f, "┼────────┼─────────┼─────────┼─────────┼─────────┼─────────┤\n");
			fprintf(f, "│Distance│%9s│%9s│%9s│%9s│%9s│\n", 
				zone_dists[0], zone_dists[1], zone_dists[2], zone_dists[3], zone_dists[4]);
			fprintf(f, "┼────────┼─────────┼─────────┼─────────┼─────────┼─────────┤\n");
			fprintf(f, "│ Time   │%9s│%9s│%9s│%9s│%9s│\n", 
				zone_time[0], zone_time[1], zone_time[2], zone_time[3], zone_time[4]);
			fprintf(f, "╰────────┴─────────┴─────────┴─────────┴─────────┴─────────╯\n");
			break;
		case ActivityStat:
			log_error("activity stat printing is not implemented yet");
			break;
	}
}
