#ifndef GARMIN_UTIL_H
#define GARMIN_UTIL_H
#include <time.h>

#define TIME_BUF_LEN 32
#define DIST_BUF_LEN 64

void fmt_dur(char(*buf)[TIME_BUF_LEN], double seconds);
void fmt_dist(char(*buf)[DIST_BUF_LEN], double meters);
long timediff(struct tm a, struct tm b);

#endif
