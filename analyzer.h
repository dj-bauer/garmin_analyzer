#ifndef GARMIN_ANALYZE_H
#define GARMIN_ANALYZE_H
#include "db.h"
#include <stdio.h>

typedef enum e_stattype {LapStat, ActivityStat} StatType;

/**
 * Contains statistics about specific heart zones
 **/
struct s_zoneinfo {
	double distance;
	double time;
};

struct s_statistics {
	StatType type;
	/*
	 * Pointer back to the objects, that
	 */
	union {
		struct s_lap* lap;
		struct s_activity* activity;
	};
	struct s_profile* profile;
	struct s_zoneinfo zonestats[5];
};

void analyze(struct s_statistics* stat, StatType type, void* device, struct s_profile* profile);
void fprint_stat(FILE* f, struct s_statistics* stat);
void print_stat(struct s_statistics* stat);

#endif
