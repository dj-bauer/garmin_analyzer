#ifndef GARMIN_PROFILE_H
#define GARMIN_PROFILE_H

struct s_pulse_zones {
	int zone_max[5];
};

struct s_profile {
	const char* name;
	struct s_pulse_zones zones;
	int resting_pulse;
	int max_pulse;
};

void calc_pulse_zones(struct s_profile* profile, int resting_pulse, int max_pulse);

#endif
