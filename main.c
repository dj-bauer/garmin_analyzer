#define _XOPEN_SOURCE 700
#include <expat.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "logger.h"
#include "osm.h"

#include "profile.h"
#include "analyzer.h"
#include "parser.h"

void start(void* data, const char* el, const char** attr)
{
	struct s_database* db = (struct s_database*) data;

	if(try_tag(el, DATABASE_TAG, db, UNDEFINED_S, DATABASE_S)) {}
	else if(!strcmp(el, ACTIVITY_TAG)) {
		// Activity construction
        struct s_activity act = {0, OTHER};
        init_vector_lap(&act.laps);
		for(int i=0; attr[i]; attr += 2) {
			if(!strcmp(attr[i], "Sport")) {
				if(!strcmp(attr[i+1], "Running")) {
					act.sport = RUNNING;
				} else if(!strcmp(attr[i+1], "Biking")) {
					act.sport = BIKING;
				} else {
					fprintf(stderr, "unknown type for activity: `%s`\n", attr[i+1]);
					act.sport = OTHER;
				}
			}
		}
		vector_append_act(&db->activities, act);
        db_push_state(db, ACTIVITY_S);
	} else if(try_tag(el, ID_TAG, db, ACTIVITY_S, ACTIVITY_ID_S)) {}
    else if(!strcmp(el, LAP_TAG)) {
        if(db_get_state(db) == ACTIVITY_S) {
            // Lap construction
            struct s_lap lap = {0, -1, -1, -1, -1, -1};
            for(int i=0; attr[i]; attr += 2) {
                if(!strcmp(attr[i], "StartTime")) {
                    if(strptime(attr[i+1], TIME_FMT, &lap.start_time) == NULL) {
                        log_warn("Could not parse lap start `%s` as `%%Y-%%m-%%dT%%XZ` timestamp", attr[i+1]);
                    }
                }
            }
			init_vector_tp(&lap.track.points);
            vector_append_lap(&db_last_activity(db)->laps, lap);
            db_push_state(db, LAP_S);
        } else {
            log_warn("Got `"LAP_TAG"` tag in state %d", db_get_state(db));
        }
    } else if(!strcmp(el, DISTANCE_TAG)) {
        if(db_get_state(db) == LAP_S)
            db_push_state(db, DISTANCE_S);
		else if(db_get_state(db) == TRACKPOINT_S)
			db_push_state(db, TRACKPOINT_DIST_S);
        else
            log_warn("Got `%s` tag in state %d", el, db_get_state(db));
	} else if(try_tag(el, CALORIES_TAG, db, LAP_S, CALORIES_S)) {}
	else if(try_tag(el, TOTALTIME_TAG, db, LAP_S, TOTALTIME_S)) {}
	else if(try_tag(el, AVGHR_TAG, db, LAP_S, AVGHR_S)) {}
	else if(try_tag(el, MAXHR_TAG, db, LAP_S, MAXHR_S)) {}
    else if(!strcmp(el, VALUE_TAG)) {
		enum e_state state = db_get_state(db);
		if(state == MAXHR_S)
			db_push_state(db, MAXHR_V_S);
		else if(state == AVGHR_S)
			db_push_state(db, AVGHR_V_S);
		else if(state == TRACKPOINT_HR_S)
			db_push_state(db, TRACKPOINT_HR_V_S);
		else
            log_warn("Got `%s` tag in state %d", el, db_get_state(db));
	} else if(try_tag(el, TRACK_TAG, db, LAP_S, TRACK_S)) {}
	else if(!strcmp(el, TRACKPOINT_TAG)) {
		if(db_get_state(db) == TRACK_S) {
			// Trackpoint construction
			struct s_track* track = db_last_track(db);
			struct s_trackpoint point = {};
			vector_append_tp(&track->points, point);
			db_push_state(db, TRACKPOINT_S);
		} else
            log_warn("Got `%s` tag in state %d", el, db_get_state(db));
	} else if(try_tag(el, TIME_TAG, db, TRACKPOINT_S, TRACKPOINT_TIME_S)) {}
	else if(try_tag(el, HR_TAG, db, TRACKPOINT_S, TRACKPOINT_HR_S)) {}
	else if(try_tag(el, ALTITUDE_TAG, db, TRACKPOINT_S, TRACKPOINT_ALT_S)) {}
	else if(try_tag(el, POSITION_TAG, db, TRACKPOINT_S, TRACKPOINT_POS_S)) {}
	else if(try_tag(el, LAT_TAG, db, TRACKPOINT_POS_S, TRACKPOINT_LAT_S)) {}
	else if(try_tag(el, LON_TAG, db, TRACKPOINT_POS_S, TRACKPOINT_LON_S)) {}
}

void element(void* data, const XML_Char* content, int len)
{
    struct s_database* db = (struct s_database*) data;
    if(db_get_state(db) == ACTIVITY_ID_S) {
        struct s_activity* activity = db_last_activity(db);
        if(strptime(content, TIME_FMT, &activity->id) == NULL) {
            log_warn("Could not parse activities id `%.*s` as `%%Y-%%m-%%dT%%XZ` timestamp", len, content);
        }
    } else if(db_get_state(db) == TOTALTIME_S) {
        // TODO: can this cause an exception?
        db_last_lap(db)->total_time_sec = strtod(content, NULL);
    } else if(db_get_state(db) == DISTANCE_S) {
        // TODO: can this cause an exception?
        db_last_lap(db)->distance_meter = strtod(content, NULL);
    } else if(db_get_state(db) == TRACKPOINT_DIST_S) {
        // TODO: can this cause an exception?
		db_last_tp(db)->distance = strtod(content, NULL);
    } else if(db_get_state(db) == CALORIES_S) {
        // TODO: can this cause an exception?
        db_last_lap(db)->calories = atoi(content);
    } else if(db_get_state(db) == AVGHR_V_S) {
        db_last_lap(db)->avg_hr = atoi(content);
    } else if(db_get_state(db) == MAXHR_V_S) {
        db_last_lap(db)->max_hr = atoi(content);
    } else if(db_get_state(db) == TRACKPOINT_HR_V_S) {
        db_last_tp(db)->heart_rate = atoi(content);
    } else if(db_get_state(db) == TRACKPOINT_ALT_S) {
        db_last_tp(db)->altitude = strtod(content, NULL);
	} else if(db_get_state(db) == TRACKPOINT_TIME_S) {
		strncat(db->parser_buffer, content, len); // TODO: Eigentlich müssen wir das hier immer machen...
	} else if(db_get_state(db) == TRACKPOINT_LAT_S) {
		db_last_tp(db)->position.lat = strtod(content, NULL);
	} else if(db_get_state(db) == TRACKPOINT_LON_S) {
		db_last_tp(db)->position.lon = strtod(content, NULL);
	}
}

void end(void* data, const char* el)
{
	struct s_database* db = (struct s_database*) data;
	if(try_close_tag(el, DATABASE_TAG, db, DATABASE_S)) {}
	else if(try_close_tag(el, ACTIVITY_TAG, db, ACTIVITY_S)) {}
	else if(try_close_tag(el, ID_TAG, db, ACTIVITY_ID_S)) {}
	else if(try_close_tag(el, LAP_TAG, db, LAP_S)) {}
	else if(try_close_tag(el, TOTALTIME_TAG, db, TOTALTIME_S)) {}
    else if (!strcmp(el, DISTANCE_TAG)) {
		enum e_state state = db_get_state(db);
        assert(state == DISTANCE_S || state == TRACKPOINT_DIST_S);
        db_pop_state(db);
	} else if(try_close_tag(el, CALORIES_TAG, db, CALORIES_S)) {}
	else if(try_close_tag(el, AVGHR_TAG, db, AVGHR_S)) {}
	else if(try_close_tag(el, MAXHR_TAG, db, MAXHR_S)) {}
	else if (!strcmp(el, VALUE_TAG)) {
		enum e_state state = db_get_state(db);
		// We currently don't use other tags named value
        if (state == AVGHR_V_S || state == MAXHR_V_S || state == TRACKPOINT_HR_V_S) 
			db_pop_state(db);
		else
			log_warn("Closing %s tag in state %d", el, state);
	} else if(try_close_tag(el, TRACK_TAG, db, TRACK_S)) {}
	else if(try_close_tag(el, TRACKPOINT_TAG, db, TRACKPOINT_S)) {}
	else if(try_close_tag(el, HR_TAG, db, TRACKPOINT_HR_S)) {}
	else if(try_close_tag(el, ALTITUDE_TAG, db, TRACKPOINT_ALT_S)) {}
	else if(try_close_tag(el, POSITION_TAG, db, TRACKPOINT_POS_S)) {}
	else if(try_close_tag(el, LAT_TAG, db, TRACKPOINT_LAT_S)) {}
	else if(try_close_tag(el, LON_TAG, db, TRACKPOINT_LON_S)) {}
	else if(try_close_tag(el, TIME_TAG, db, TRACKPOINT_TIME_S)) {
		struct s_trackpoint* tp = db_last_tp(db);
        if(strptime(db->parser_buffer, TIME_FMT, &tp->time) == NULL) {
            log_warn("Could not parse Trackpoints time `%s` as `%%Y-%%m-%%dT%%XZ` timestamp", db->parser_buffer);
        }
		// Clear the parser buffer
		db->parser_buffer[0] = '\0';
	}
}

int main(int argc, const char* argv[])
{
	if(argc < 2) {
		fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
		return 1;
	}

	XML_Parser parser = XML_ParserCreate(NULL);
	if(!parser) {
		fprintf(stderr, "Failed to create parser\n");
		return 1;
	}

	FILE* stream = fopen(argv[1], "r");
	if(!stream) {
		fprintf(stderr, "Couldn't open file %s\n", argv[1]);
		XML_ParserFree(parser);
		return 1;
	}

	int done;
	struct s_database db;
	init_db(&db);


	XML_SetUserData(parser, &db);
	XML_SetElementHandler(parser, start, end);
    XML_SetCharacterDataHandler(parser, element);

	do {
		void* const buf = XML_GetBuffer(parser, BUFSIZ);
		if(!buf) {
			log_error("Couldn't grab buffer");
			XML_ParserFree(parser);
			return 1;
		}
		const size_t len = fread(buf, 1, BUFSIZ, stream);

		if(ferror(stream)) {
            log_error("Read error");
			XML_ParserFree(parser);
			return 1;
		}
		done = feof(stream);

		if(XML_ParseBuffer(parser, (int) len, done) == XML_STATUS_ERROR) {
			fprintf(stderr, "Parse error at line %lu\n%s\n",
					XML_GetCurrentLineNumber(parser),
					XML_ErrorString(XML_GetErrorCode(parser)));
			XML_ParserFree(parser);
			return 1;
		}
	} while(!done);
	XML_ParserFree(parser);
	fclose(stream);
    log_debug("Parsing done :)");

	for(int i=0; i<vector_size_act(&db.activities); i++) {
		const struct s_activity* act = vector_at_act(&db.activities, i);
		log_info("Activity type %s starting %s", activity_sport_name(act->sport), asctime(&act->id));
        printf("Laps:\n");
        for(int i=0; i<vector_size_lap(&act->laps); i++) {
            const struct s_lap* lap = vector_at_lap((Vector_lap*) &act->laps, i);
            print_lap(lap);
        }
	}

	struct s_osm map;
	osm_load("kennedy.osm", &map);

	// Plot route
	size_t tp_index = 999942069690;
	for(int i=0; i<vector_size_act(&db.activities); i++) {
		struct s_activity* act = vector_at_act(&db.activities, i);
		for(int j=0; j<vector_size_lap(&act->laps); j++) {
			struct s_lap* lap = vector_at_lap(&act->laps, j);
			struct s_track* track = &lap->track;
			struct s_osmway* way = osm_add_way(&map, tp_index++, vector_size_tp(&track->points), false,
					(struct s_wayattr) {.type=CustomType, .subtypes.custom = {.color="#ff0000", .name="track"}});
			for(int k=0; k<vector_size_tp(&track->points); k++) {
				struct s_trackpoint* tp = vector_at_tp(&track->points, k);
				struct s_osmnode* osmnode = osm_add_node(&map, tp_index++, (coord) {tp->position.lat, tp->position.lon}, 
						(struct s_wayattr) {.type=CustomType, .subtypes.custom={.color="#005000", .name="trackpoint"}});
				way->nodes[k] = osmnode;
			}
		}
	}
	// End plotting route

	log_info("Plotting");
	osm_graph(&map, "route.svg", NULL);
	log_debug("Cleaning up");
	osm_free(&map);

	struct s_statistics run;
	struct s_profile me = (struct s_profile) {.name = "Daniel"};					
	calc_pulse_zones(&me, 50, 195);
	analyze(&run, LapStat, vector_at_lap(&vector_first_act(&db.activities)->laps, 1), &me);
	print_stat(&run);

	deinit_db(&db);
	return 0;
}
