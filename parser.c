#include "parser.h"
#include <string.h>
#include "logger.h"

bool try_tag(const char* tag, const char* searching, struct s_database* db, enum e_state old_state, enum e_state new_state)
{
	if(!strcmp(tag, searching)) {
		if(db_get_state(db) == old_state) {
			db_push_state(db, new_state);
			return true;
		} else
			log_warn("Got `%s` tag in state %d", tag, db_get_state(db));
	}
	return false;
}

bool try_close_tag(const char* tag, const char* searching, struct s_database* db, enum e_state expected_state)
{
	if(!strcmp(tag, searching)) {
		if(db_get_state(db) == expected_state) {
			db_pop_state(db);
			return true;
		} else
			log_warn("Closing `%s` tag in invalid state %d", tag, db_get_state(db));
	}
	return false;
}
