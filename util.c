#include "util.h"
#include <stdio.h>
#include <string.h>

void fmt_dur(char(*buf)[TIME_BUF_LEN], double secs_double)
{
	if(secs_double < 60) {
		snprintf((char*) buf, TIME_BUF_LEN, "%.2lfsec", secs_double);
		return;
	}
    int seconds = (int) secs_double;
    int minutes = seconds / 60;
    int rest_secs = seconds % 60;
    if(minutes < 60) {
        snprintf((char*) buf, TIME_BUF_LEN, "%02d:%02dmin", minutes, rest_secs);
        return;
    }

    int hours = minutes / 60;
    int rest_min = minutes % 60;
    if(hours < 24) {
        snprintf((char *) buf, TIME_BUF_LEN, "%02d:%02d:%02dh", hours, rest_min, rest_secs);
        return;
    }

    strcpy((char*) buf, ">1d");
}

void fmt_dist(char(*buf)[DIST_BUF_LEN], double meters)
{
    if(meters < 1000) {
        snprintf((char*) buf, DIST_BUF_LEN, "%.2lfm", meters);
        return;
    }
    double km = meters/1000;
    snprintf((char*) buf, DIST_BUF_LEN, "%.2lfkm", km);
}

long timediff(struct tm a, struct tm b)
{
	long delta = a.tm_sec - b.tm_sec;
	delta += (a.tm_min - b.tm_min)*60;
	delta += (a.tm_hour - b.tm_hour)*3600;
	return delta;
}
